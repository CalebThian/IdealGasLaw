#include <iostream>
#include <cstdio>
#include <cmath>
#include <ctime>
#include <cstdlib>
#include <vector>
#include <exception>
using namespace std;

/*mass of an atom*/
const double m=40.0*1.667*pow(10,-27);

/*number of particles*/
const int N=5;

/*length of boundary*/
double L=2*pow(10,-7);

/*step size, or said delta t*/
double h=1*pow(10,0);

/*time for observation or simulation*/
double t=10;

/*bolztzman constant*/
double kb = 1.381*pow(10,-23);

/*speed limit in specific direction*/
double vl=L/3;

/*temperature*/
const int Trange=100;// t/h
double T[Trange];

/*certain amount of time for calculation of P*/
double tt=2;

/*pressure*/
const int Prange = 5;// t/tt
double P[Prange];

/*epsilon for Lennard-Jones Potential*/
double ep = 1.66*pow(10,-21);

/*delta for Lennard-Jones Potential*/
double de = 3.4*pow(10,-10);

/*struct of a particle, inclue its position and velocity*/
struct Particle
{
	/*position*/
	double x[Trange];
	double y[Trange];
	double z[Trange];

	/*velocity*/
	double xd[Trange];
	double yd[Trange];
	double zd[Trange];

	/*acceleration*/
	double xdd[Trange];
	double ydd[Trange];
	double zdd[Trange];

	/*hits wall boolean*/
	bool hitsWall;
};

/*new a particle array*/
Particle  p1[N];

/* create a vector which save the pressure over a certain time*/
vector <double> pressure;

void initParticle();//initialize a particle with position and velocity
void checkGivenValue();//printf given value
void printParticleProp(int a,int b);//printf specific particle property
void calTemperature(int b);//calculate instant temperature
void calPressure(int c);//calculate a particular time interval average pressure
void generalCal(int a,int b);//calculate a specific particle's position,velocity and acceleration
double calSca(int a,int b);//calculate a specific particle's scalar of its vector
void calHitWall(int a,int b,char position);//calculation when the particle hits the wall
double myAbs(double velocity);//absolute a double data
void ETmethod(int a,int b);//Euler's method and 2nd-Order Taylor Method
void Vmethod(int a,int b);//verlet algorithm
double calF(int a,int b);//calculate resultant force on a particle
double calD(int a,int b,int c);//calculate distance between two particle

int main()
{
	T[0]=273;
	P[0]=0;
	double detCallCalPressure=tt/h;
	srandom(time(NULL));
	/*checkGivenValue();*/
	initParticle();
	/*
	for(int i=0;i<N;++i)
		printParticleProp(i,0);
	*/
	for(int i=0;i<Trange-1;++i)
	{
		cout<<"i = "<<i<<endl;
		try{
		for(int j=0;j<N;++j)
			generalCal(j,i);
		calTemperature(i);
		cout<<"here"<<endl;
		if(i%int(tt/h)==0)
			calPressure(i/int(tt/h));
		}catch(exception&){
			cout<<"error occurs when i = "<<i<<endl;
		}
	}
}

void initParticle()
{
	/*as rand() is a int function, we use it by change the initial value to int and then *pow(10,??)to get the approximate value*/
	int Lb = L*pow(10,14);
	int vlb = vl*pow(10,-2*log10(vl));
	cout<<"vlb = "<<vlb<<endl;
	for(int i=0;i<N;++i)
	{
		p1[i].x[0]=rand()%Lb*pow(10,-14)-L/2;
		p1[i].y[0]=rand()%Lb*pow(10,-14)-L/2;
		p1[i].z[0]=rand()%Lb*pow(10,-14)-L/2;

		p1[i].xd[0]=rand()%vlb*pow(10,2*log10(vl));
		p1[i].yd[0]=rand()%vlb*pow(10,2*log10(vl));
		p1[i].zd[0]=rand()%vlb*pow(10,2*log10(vl));

		p1[i].xdd[0]=0;
		p1[i].ydd[0]=0;
		p1[i].zdd[0]=0;

		/*initialize array*/
		p1[i].hitsWall=1;//1 because at begin, use Euler's method and 2nd-Order Taylor Method
	}
}

void checkGivenValue()
{
	printf("m = ");
	printf("%.4e",m);
	printf("\n");

	printf("N = ");
	printf("%d",N);
    printf("\n");

	printf("L = ");
	printf("%.4e",L);
	printf("\n");

	printf("h = ");
	printf("%.4e",h);
	printf("\n");

	printf("t = ");
	printf("%.4e",t);
	printf("\n");

	printf("kb = ");
	printf("%.4e",kb);
	printf("\n");

	printf("vl = ");
	printf("%.4e",vl);
	printf("\n");

	printf("T = ");
	printf("%.4e",T[0]);
	printf("\n");

	printf("P = ");
	printf("%.4e",P[0]);
	printf("\n");

	printf("epsilon, ep = ");
	printf("%.4e",ep);
	printf("\n");

	printf("delta, de = ");
	printf("%.4e",de);
	printf("\n");
}

void printParticleProp(int a,int b)
{
	printf("x = ");
	printf("%.4e",p1[a].x[b]);
	printf("\n");

	printf("y = ");
	printf("%.4e",p1[a].y[b]);
	printf("\n");

	printf("z = ");
	printf("%.4e",p1[a].z[b]);
	printf("\n");

	printf("v_x = ");
	printf("%.4e",p1[a].xd[b]);
	printf("\n");

	printf("v_y = ");
	printf("%.4e",p1[a].yd[b]);
	printf("\n");

	printf("v_z = ");
	printf("%.4e",p1[a].zd[b]);
	printf("\n");
}

void calTemperature(int b)
{
	double Ek=0;

	/*calculate the total kinetic energy*/
	for(int i=0;i<N;++i)
	{
		Ek+=0.5*m*pow(pow(p1[i].xd[b],2)+pow(p1[i].yd[b],2)+pow(p1[i].zd[b],2),1);
	}

	/*1.5*kb*T=Ek*/
	T[b]=Ek/kb/1.5;
}

void calPressure(int c)
{
	P[c]=0;//initialize P
	/*summation of pressure*/
	for(int i=0;i<pressure.size();++i)
		P[c]+=pressure.at(i);

	P[c]/=(tt*L*L);

	pressure.clear();//clear pressure
}

void generalCal(int a,int b)
{
	//if collision with wall:r(t)=r(t)/scalar of r(t)*2*L-r(t),v(t)=-v(t)
	//verlet algorithm:r(t+h)=2*r(t)-r(t-h)+h*h*F(r,v,t)/m;
	//verlet algorithm:v(t)=(r(t+h)-r(t-h))/2/h
	//Lennard-Jones Force:F(r)=ep/de*[12*pow((de/r),13)-6*pow((de/r),7)];
	//Lennard-Jones Potential:U(r)=4*ep[pow((de/r),12)-pow((de/r),6)]
	
	/*First, process if the particle hits the wall*/
	if(p1[a].x[b]>L/2 || p1[a].x[b]<L/2)
		calHitWall(a,b,'x');
	else if(p1[a].y[b]>L/2 || p1[a].y[b]<L/2)
		calHitWall(a,b,'y');
	else if(p1[a].z[b]>L/2 || p1[a].z[b]<L/2)
		calHitWall(a,b,'z');

	/*Now, calculate the next h position,velocity and acceleration*/
	/*At begin or after hit wall, use Euler's method or 2nd-Order Taylor Method*/
	if(p1[a].hitsWall==1)
	{
		ETmethod(a,b);
		p1[a].hitsWall=0;
	}
	else
		Vmethod(a,b);
}

double calSca(int a,int b)
{
	double sqx = pow(p1[a].x[b],2);
	double sqy = pow(p1[a].y[b],2);
	double sqz = pow(p1[a].z[b],2);
	double sq = pow(sqx+sqy+sqz,0.5);
	return sq;
}

void calHitWall(int a,int b,char position)
{
	p1[a].hitsWall=1;
	if(position == 'x')
	{
		p1[a].x[b]=p1[a].x[b]/calSca(a,b)*2*L-p1[a].x[b];
		pressure.push_back(2*m*p1[a].xd[b]);
		p1[a].xd[b]*=-1;
	}
	else if(position == 'y')
	{
		p1[a].y[b]=p1[a].y[b]/calSca(a,b)*2*L-p1[a].y[b];
		pressure.push_back(2*m*p1[a].yd[b]);
		p1[a].yd[b]*=-1;
	}
	else if(position == 'z')
	{
		p1[a].z[b]=p1[a].z[b]/calSca(a,b)*2*L-p1[a].z[b];
		pressure.push_back(2*m*p1[a].zd[b]);
		p1[a].zd[b]*=-1;
	}
}

double myAbs(double velocity)
{
	if(velocity<0)
		return velocity*-1;
	else
		return velocity;
}

void ETmethod(int a,int b)
{
	p1[a].x[b+1]=p1[a].x[b]+h*p1[a].xd[b]+h*h*p1[a].xdd[b];
	p1[a].y[b+1]=p1[a].y[b]+h*p1[a].yd[b]+h*h*p1[a].ydd[b];
	p1[a].z[b+1]=p1[a].z[b]+h*p1[a].zd[b]+h*h*p1[a].zdd[b];

	p1[a].xd[b+1]=p1[a].xd[b]+h*p1[a].xdd[b];
	p1[a].yd[b+1]=p1[a].yd[b]+h*p1[a].ydd[b];
	p1[a].zd[b+1]=p1[a].zd[b]+h*p1[a].zdd[b];

	//Lennard-Jones Force:F(r)=ep/de*[12*pow((de/r),13)-6*pow((de/r),7)];
	cout<<"before"<<endl;
	double F=calF(a,b);
	cout<<"F"<<endl;
	double sVector=calSca(a,b);
	cout<<"before?"<<endl;
	p1[a].xdd[b+1]=F*p1[a].x[b]/sVector/m;
	cout<<"after"<<endl;
	p1[a].ydd[b+1]=F*p1[a].y[b]/sVector/m;
	p1[a].zdd[b+1]=F*p1[a].z[b]/sVector/m;
}

void Vmethod(int a,int b)
{
	//verlet algorithm:r(t+h)=2*r(t)-r(t-h)+h*h*F(r,v,t)/m;
	//verlet algorithm:v(t)=(r(t+h)-r(t-h))/2/h
	double F=calF(a,b);
	p1[a].x[b+1]=2*p1[a].x[b]-p1[a].x[b-1]+h*h*F/m;
	p1[a].y[b+1]=2*p1[a].y[b]-p1[a].y[b-1]+h*h*F/m;
	p1[a].z[b+1]=2*p1[a].z[b]-p1[a].z[b-1]+h*h*F/m;

	p1[a].xd[b+1]=(p1[a].x[b+1]-p1[a].x[b-1])/2/h;
	p1[a].yd[b+1]=(p1[a].y[b+1]-p1[a].y[b-1])/2/h;
	p1[a].zd[b+1]=(p1[a].z[b+1]-p1[a].z[b-1])/2/h;

	
}

double calF(int a,int b)
{
	double F=0;
	for(int i=0;i<Trange;++i)
	{
		double r=calD(a,b,i);
		F=F+ep/de*(12*pow((de/r),13)-6*pow((de/r),7));
		cout<<"Trange = "<<Trange<<endl;
		cout<<"F = "<<F<<endl;
		cout<<"r = "<<r<<endl;
	}
	return F;
}

double calD(int a,int b,int c)
{
	double d=0;
	double sqx=pow(p1[a].x[b]-p1[c].x[b],2);
	double sqy=pow(p1[a].y[b]-p1[c].y[b],2);
	double sqz=pow(p1[a].z[b]-p1[c].z[b],2);
	d=pow(sqx+sqy+sqz,0.5);
	return d;
}
